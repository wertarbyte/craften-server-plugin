package de.craften.plugins.craften.skyblock.components;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.components.PluginComponent;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.util.Vector;

public class FireBreathingDragon implements PluginComponent {

    @Override
    public void activateFor(final CraftenServerPlugin plugin) {
        plugin.getServer().getScheduler().runTaskTimer(plugin, new Runnable() {
            @Override
            public void run() {
                throwFlames(new Location(plugin.getServer().getWorld("skyworld"), 0.5, 48.75, 3.5), new Vector(0, 0, 1), 7, 5);
            }

            private void throwFlames(final Location location, final Vector direction, final int length, int repeat) {
                for (int r = 0; r < repeat; r++) {
                    plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
                        @Override
                        public void run() {
                            throwFlames(location, direction, length);
                        }
                    }, r * 5);
                }
            }

            private void throwFlames(final Location location, final Vector direction, final int length) {
                for (int d = 0; d < length * 4; d++) {
                    final Vector offset = direction.clone().multiply(d * 0.5).add(Vector.getRandom().normalize().subtract(new Vector(0.5, 0.5, 0.5)).multiply(0.2 * d));
                    plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
                        @Override
                        public void run() {
                            location.getWorld().playEffect(location.clone().add(offset), Effect.MOBSPAWNER_FLAMES, 0);
                        }
                    }, d / 2);
                }
            }
        }, 0, 20 * 3);
    }
}
