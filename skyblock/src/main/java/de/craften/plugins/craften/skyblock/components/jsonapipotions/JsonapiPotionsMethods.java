package de.craften.plugins.craften.skyblock.components.jsonapipotions;

import com.alecgorge.minecraft.jsonapi.dynamic.API_Method;
import com.alecgorge.minecraft.jsonapi.dynamic.JSONAPIMethodProvider;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

public class JsonapiPotionsMethods implements JSONAPIMethodProvider {
    @API_Method(namespace = "craften", name = "give_potion")
    public boolean givePotion(Object nickname, Object type, Object amount, Object level, Object extendedDuration, Object splash) {
        Potion potion = new Potion(PotionType.valueOf(String.valueOf(type)));
        potion.setLevel(Integer.parseInt(String.valueOf(level)));
        potion.setHasExtendedDuration(Boolean.parseBoolean(String.valueOf(extendedDuration)));
        potion.setSplash(Boolean.parseBoolean(String.valueOf(splash)));
        Player player = Bukkit.getPlayer(String.valueOf(nickname));
        return player.getInventory().addItem(potion.toItemStack(Integer.parseInt(String.valueOf(amount)))).isEmpty();
    }
}
