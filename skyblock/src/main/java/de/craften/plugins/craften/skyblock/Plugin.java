package de.craften.plugins.craften.skyblock;

import de.craften.plugins.craften.skyblock.components.FireBreathingDragon;
import de.craften.plugins.craften.skyblock.components.jsonapipotions.JsonapiPotions;
import de.craften.plugins.craften.skyblock.components.ShowIslandMenuOnJoin;
import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.components.*;
import de.craften.plugins.craftenserver.common.components.commands.ClearCommand;
import de.craften.plugins.craftenserver.common.components.commands.CreativeCommand;
import de.craften.plugins.craftenserver.common.components.commands.PaintballCommand;
import de.craften.plugins.craftenserver.common.components.commands.SpawnCommand;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Plugin extends CraftenServerPlugin {
    @Override
    public void onEnable() {
        super.onEnable();

        addComponents(
                new ClearCommand(),
                new SpawnCommand(),
                new PaintballCommand(),
                new DisableJoinQuitMessages(),
                new CreativeCommand(),
                new MaintenanceMode(),
                new Motd(),
                new AfkPlayers(),
                new BlockMobSpawnerEdit()
        );

        addComponents(
                new TeleportOnJoin(new Location(Bukkit.getWorld("skyworld"), 0.5, 37, 0.5)),
                new ShowIslandMenuOnJoin(),
                new FireBreathingDragon(),
                new JsonapiPotions()
        );
    }

    @Override
    public ServerType getType() {
        return ServerType.SKYBLOCK;
    }
}
