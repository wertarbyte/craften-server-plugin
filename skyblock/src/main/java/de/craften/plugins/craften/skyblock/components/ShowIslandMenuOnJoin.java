package de.craften.plugins.craften.skyblock.components;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.components.PluginComponent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ShowIslandMenuOnJoin implements PluginComponent, Listener {
    private CraftenServerPlugin plugin;

    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                event.getPlayer().performCommand("is"); //Automatically show skyblock menu
            }
        }, 20);
    }
}
