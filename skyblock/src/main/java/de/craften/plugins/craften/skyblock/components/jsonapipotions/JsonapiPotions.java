package de.craften.plugins.craften.skyblock.components.jsonapipotions;

import com.alecgorge.minecraft.jsonapi.JSONAPI;
import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.components.PluginComponent;

public class JsonapiPotions implements PluginComponent {
    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        JSONAPI jsonapi = (JSONAPI) plugin.getServer().getPluginManager().getPlugin("JSONAPI");
        jsonapi.registerMethods(new JsonapiPotionsMethods());
    }
}

