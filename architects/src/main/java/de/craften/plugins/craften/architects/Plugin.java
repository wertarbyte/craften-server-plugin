package de.craften.plugins.craften.architects;

import de.craften.plugins.craften.architects.components.NoFireSpread;
import de.craften.plugins.craften.architects.components.OnlyArchitects;
import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.components.DisableJoinQuitMessages;
import de.craften.plugins.craftenserver.common.components.AfkPlayers;
import de.craften.plugins.craftenserver.common.components.MaintenanceMode;
import de.craften.plugins.craftenserver.common.components.commands.ClearCommand;
import de.craften.plugins.craftenserver.common.components.commands.SpawnCommand;

public class Plugin extends CraftenServerPlugin {
    @Override
    public void onEnable() {
        super.onEnable();

        addComponents(
                new ClearCommand(),
                new SpawnCommand(),
                new DisableJoinQuitMessages(),
                new MaintenanceMode(),
                new AfkPlayers()
        );

        addComponents(
                new OnlyArchitects(),
                new NoFireSpread()
        );
    }

    @Override
    public ServerType getType() {
        return ServerType.ARCHITECTS;
    }
}
