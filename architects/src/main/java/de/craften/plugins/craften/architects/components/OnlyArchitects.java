package de.craften.plugins.craften.architects.components;

import de.craften.plugins.craftenserver.common.components.PluginComponentBase;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class OnlyArchitects extends PluginComponentBase implements Listener {
    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        if (!event.getPlayer().isWhitelisted()) {
            event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, "You are not an architect.");
        }
    }
}
