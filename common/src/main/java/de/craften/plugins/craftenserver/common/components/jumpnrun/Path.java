package de.craften.plugins.craftenserver.common.components.jumpnrun;


import de.craften.plugins.craftenserver.common.util.region.RectangularRegion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A Jump'n'Run path.
 */
public class Path {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    private File file;

    private String name;
    private List<Platform> platforms;
    private int gemReward = 0;
    private Score record;
    private Score recordToday;

    private Location recordSign;
    private Location recordTodaySign;

    /**
     * Creates a new path.
     *
     * @param name            name of the path
     * @param platforms       platforms, at least two (start and end)
     * @param recordSign      sign to display the record on
     * @param recordTodaySign sign to display today's record on
     * @param file            configuration file of this path
     */
    public Path(String name, List<Platform> platforms, Location recordSign, Location recordTodaySign, File file) {
        this.name = name;
        this.platforms = platforms;

        this.recordSign = recordSign;
        this.recordTodaySign = recordTodaySign;

        this.file = file;
    }

    /**
     * Gets the name of this path.
     *
     * @return name of this path
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the gems a player will get when finishing this path.
     *
     * @return the gems a player will get when finishing this path
     */
    public int getGemReward() {
        return gemReward;
    }

    /**
     * Gets the record score on this path.
     *
     * @return record score on this path, null if no record exists
     */
    public Score getRecord() {
        return record;
    }

    /**
     * Gets today's record score on this path. This is actually the record score on this path since
     * {@link #resetRecordToday()} was called.
     *
     * @return today's record score on this path, null if no record exists
     */
    public Score getRecordToday() {
        return recordToday;
    }

    /**
     * Resets today's record score.
     */
    public void resetRecordToday() {
        setRecordToday(null);
    }

    /**
     * Resets the record score.
     */
    public void resetRecord() {
        setRecord(null);
    }

    /**
     * Sets today's record score.
     *
     * @param score new record
     */
    public void setRecordToday(Score score) {
        recordToday = score;

        if (recordTodaySign != null) {
            BlockState signBlock = recordTodaySign.getBlock().getState();
            if (signBlock instanceof Sign) {
                Sign sign = ((Sign) signBlock);
                if (score == null) {
                    sign.setLine(1, "");
                    sign.setLine(2, "");
                } else {
                    sign.setLine(1, score.getFormattedTime());
                    sign.setLine(2, score.getNickname());
                }
                sign.update(true);
            }
        }
    }

    /**
     * Sets the record.
     *
     * @param score new record
     */
    public void setRecord(Score score) {
        record = score;

        if (recordSign != null) {
            BlockState signBlock = recordSign.getBlock().getState();
            if (signBlock instanceof Sign) {
                Sign sign = ((Sign) signBlock);
                if (score == null) {
                    sign.setLine(1, "");
                    sign.setLine(2, "");
                    sign.setLine(3, "");
                } else {
                    sign.setLine(1, score.getFormattedTime());
                    sign.setLine(2, score.getNickname());
                    sign.setLine(3, DATE_FORMAT.format(score.getDate()));
                }
                sign.update(true);
            }
        }
    }

    /**
     * Gets this track's start platform.
     *
     * @return start platform of this track
     */
    public Platform getStart() {
        return platforms.get(0);
    }

    /**
     * Gets this track's end platform.
     *
     * @return end platform of this track
     */
    protected Platform getEnd() {
        return platforms.get(platforms.size() - 1);
    }

    /**
     * Gets the platform with the given index. 0 is the start platform.
     *
     * @param index index of the platform
     * @return the platform with the given index or null if a platform with that index doesn't exist
     */
    public Platform getPlatform(int index) {
        if (index < 0 || index >= platforms.size()) {
            return null;
        }
        return platforms.get(index);
    }

    /**
     * Gets the number of platforms of this path.
     *
     * @return number of platforms of this path
     */
    public int getPlatformCount() {
        return platforms.size();
    }

    /**
     * Checks if the given player is at the start of this path.
     *
     * @param player player
     * @return true if the given player is at the start of this path, false if not
     */
    public boolean isAtStart(Player player) {
        return getStart().isInside(player.getLocation());
    }

    /**
     * Checks if the given player is at the end of this path.
     *
     * @param player player
     * @return true if the given player is at the end of this path, false if not
     */
    public boolean isAtEnd(Player player) {
        return getEnd().isInside(player.getLocation());
    }

    /**
     * Gets the world this path is located in.
     *
     * @return world this path is located in
     */
    public World getWorld() {
        return getStart().getWorld();
    }

    /**
     * Saves this path's records into this path's configuration file.
     *
     * @throws java.io.IOException if saving the configuration file fails
     */
    public void saveRecords() throws IOException {
        YamlConfiguration pathConfig = YamlConfiguration.loadConfiguration(file);
        if (record != null) {
            pathConfig.set("record.nickname", record.getNickname());
            pathConfig.set("record.date", record.getDate().getTime());
            pathConfig.set("record.time", record.getTime());
        } else {
            pathConfig.set("record.nickname", "");
            pathConfig.set("record.date", 0);
            pathConfig.set("record.time", 0);
        }

        if (recordToday != null) {
            pathConfig.set("recordToday.nickname", recordToday.getNickname());
            pathConfig.set("recordToday.date", recordToday.getDate().getTime());
            pathConfig.set("recordToday.time", recordToday.getTime());
        } else {
            pathConfig.set("recordToday.nickname", "");
            pathConfig.set("recordToday.date", 0);
            pathConfig.set("recordToday.time", 0);
        }
        pathConfig.save(file);
    }

    /**
     * Saves this path into this path's configuration file.
     *
     * @throws java.io.IOException if saving the configuration file fails
     */
    public void save() throws IOException {
        YamlConfiguration pathConfig = YamlConfiguration.loadConfiguration(file);

        pathConfig.set("name", name);
        pathConfig.set("gemReward", gemReward);
        pathConfig.set("world", getWorld());
        pathConfig.set("checkpoints", platforms.stream().map((p) -> {
            Map<String, Object> checkpoint = new HashMap<>();
            checkpoint.put("x1", p.getRegion().getXMin());
            checkpoint.put("z1", p.getRegion().getZMin());
            checkpoint.put("x2", p.getRegion().getXMax());
            checkpoint.put("z2", p.getRegion().getZMax());
            checkpoint.put("y", p.getRegion().getY());
            return checkpoint;
        }).collect(Collectors.toList()));

        if (recordSign != null) {
            pathConfig.set("recordSign.world", recordSign.getWorld().getName());
            pathConfig.set("recordSign.x", recordSign.getBlockX());
            pathConfig.set("recordSign.y", recordSign.getBlockY());
            pathConfig.set("recordSign.z", recordSign.getBlockZ());
        }
        if (recordTodaySign != null) {
            pathConfig.set("recordTodaySign.world", recordTodaySign.getWorld().getName());
            pathConfig.set("recordTodaySign.x", recordTodaySign.getBlockX());
            pathConfig.set("recordTodaySign.y", recordTodaySign.getBlockY());
            pathConfig.set("recordTodaySign.z", recordTodaySign.getBlockZ());
        }

        pathConfig.save(file);
        saveRecords();
    }

    /**
     * Loads a path from the given file.
     *
     * @param pathFile path configuration file in YAML format
     * @return the loaded path
     */
    public static Path load(File pathFile) {
        ConfigurationSection pathConfig = YamlConfiguration.loadConfiguration(pathFile);
        List<Map<?, ?>> checkpoints = pathConfig.getMapList("checkpoints");
        List<Platform> platforms = new ArrayList<>(checkpoints.size());

        for (Map checkpoint : checkpoints) {
            platforms.add(new Platform(
                    pathConfig.getString("world", ""),
                    new RectangularRegion(
                            (Integer) checkpoint.get("x1"),
                            (Integer) checkpoint.get("z1"),
                            (Integer) checkpoint.get("x2"),
                            (Integer) checkpoint.get("z2"),
                            (Integer) checkpoint.get("y")
                    )
            ));
        }

        Path path = new Path(
                pathConfig.getString("name", ""),
                platforms,
                pathConfig.contains("recordSign") ? new Location(
                        Bukkit.getWorld(pathConfig.getString("recordSign.world")),
                        pathConfig.getInt("recordSign.x", 0),
                        pathConfig.getInt("recordSign.y", 0),
                        pathConfig.getInt("recordSign.z", 0)
                ) : null,
                pathConfig.contains("recordTodaySign") ? new Location(
                        Bukkit.getWorld(pathConfig.getString("recordTodaySign.world")),
                        pathConfig.getInt("recordTodaySign.x", 0),
                        pathConfig.getInt("recordTodaySign.y", 0),
                        pathConfig.getInt("recordTodaySign.z", 0)
                ) : null,
                pathFile
        );


        if (!pathConfig.getString("record.nickname", "").isEmpty()) {
            path.setRecord(new Score(
                    pathConfig.getString("record.nickname"),
                    path,
                    new Date(pathConfig.getLong("record.date", 0)),
                    pathConfig.getLong("record.time", 0)
            ));
        }
        if (!pathConfig.getString("recordToday.nickname", "").isEmpty()) {
            path.setRecordToday(new Score(
                    pathConfig.getString("recordToday.nickname"),
                    path,
                    new Date(pathConfig.getLong("recordToday.date", 0)),
                    pathConfig.getLong("recordToday.time", 0)
            ));
        }

        path.gemReward = pathConfig.getInt("gemReward", 0);

        return path;
    }
}
