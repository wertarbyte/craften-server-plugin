package de.craften.plugins.craftenserver.common.util.region;

import org.bukkit.Location;

/**
 * A rectangular region defined by two corners.
 */
public class RectangularRegion {
    private final int x1;
    private final int z1;
    private final int x2;
    private final int z2;
    private final int y;

    //corners are (x1,z1) and (x2,z2) where x1<=x2 and z1<=z2

    public RectangularRegion(PointXZ a, PointXZ b, int y) {
        x1 = Math.min(a.getX(), b.getX());
        x2 = Math.max(a.getX(), b.getX());

        z1 = Math.min(a.getZ(), b.getZ());
        z2 = Math.max(a.getZ(), b.getZ());

        this.y = y;
    }

    public RectangularRegion(int x1, int z1, int x2, int z2, int y) {
        this.x1 = Math.min(x1, x2);
        this.z1 = Math.min(z1, z2);
        this.x2 = Math.max(x1, x2);
        this.z2 = Math.max(z1, z2);
        this.y = y;
    }

    public static RectangularRegion byStartAndSize(int x, int y, int z, int width, int length) {
        return new RectangularRegion(x, z, x + width - 1, z + length - 1, y);
    }

    public boolean contains(Location location) {
        return x1 <= location.getBlockX() && x2 >= location.getBlockX() && z1 <= location.getBlockZ() && z2 >= location.getBlockZ() && location.getBlockY() == getY();
    }

    public int getXMin() {
        return x1;
    }

    public int getXMax() {
        return x2;
    }

    public int getZMin() {
        return z1;
    }

    public int getZMax() {
        return z2;
    }

    public int getY() {
        return y;
    }

    public boolean contains(int x, int z) {
        return x1 <= x && x2 >= x && z1 <= z && z2 >= z;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RectangularRegion) {
            RectangularRegion other = (RectangularRegion) obj;
            return other.getXMin() == getXMin() && other.getXMax() == getXMax() &&
                    other.getZMin() == getZMin() && other.getZMax() == getZMax() &&
                    other.getY() == getY();
        } else {
            return super.equals(obj);
        }
    }

    @Override
    public String toString() {
        return String.format("RectangularRegion((%d, %d), (%d, %d), %d)", getXMin(), getZMin(), getXMax(), getZMax(), getY());
    }
}
