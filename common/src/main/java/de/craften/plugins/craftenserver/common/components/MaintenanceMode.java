package de.craften.plugins.craftenserver.common.components;

import de.craften.plugins.craftenserver.common.ServerType;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * Adds a maintenance mode to the server.
 */
public class MaintenanceMode extends PluginComponentBase implements CommandExecutor, Listener {
    private boolean isInMaintenance = false;

    @Override
    protected void onActivated() {
        registerCommand("maintenance", this);

        if (getConfig().getBoolean("maintenance.enabled", false)) {
            enableMaintenance();
        }
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if (commandSender.hasPermission("csp.maintenance")) {
            if (isInMaintenance) {
                disableMaintenance();
                commandSender.sendMessage("Maintenance mode disabled.");
            } else {
                enableMaintenance();
                commandSender.sendMessage("Maintenance mode enabled, all players kicked.");
                getConfig().set("maintenance.enabled", true);
                saveConfig();
            }
            return true;
        } else {
            return false;
        }
    }

    private void enableMaintenance() {
        isInMaintenance = true;
        getServer().broadcastMessage(ChatColor.RED + "We are performing some maintenance work on this server. You'll now be teleported to the lobby.");

        for (Player p : getServer().getOnlinePlayers()) {
            teleportPlayer(p, ServerType.LOBBY);
        }
    }

    private void disableMaintenance() {
        isInMaintenance = false;
        getConfig().set("maintenance.enabled", false);
        saveConfig();
    }

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        if (isInMaintenance && !event.getPlayer().isOp()) {
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Server maintenance. Please try again later!");
        }
    }
}
