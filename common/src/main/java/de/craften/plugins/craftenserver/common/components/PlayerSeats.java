package de.craften.plugins.craftenserver.common.components;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Stairs;
import org.bukkit.util.Vector;

/**
 * Adds seats for players.
 */
public class PlayerSeats extends PluginComponentBase implements Listener {
    private void sit(Block seat, BlockFace face, Player player) {
        ArmorStand armorStand = seat.getWorld().spawn(
                seat.getLocation().add(0.5, -1.25, 0.5).setDirection(new Vector(face.getModX(), face.getModY(), face.getModZ()))
                , ArmorStand.class);
        armorStand.setVisible(false);
        armorStand.setSmall(true);
        armorStand.setGravity(false);
        armorStand.setPassenger(player);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && !event.getPlayer().isSneaking()) {
            Block block = event.getClickedBlock().getLocation().getBlock();
            MaterialData data = block.getState().getData();
            if (data instanceof Stairs) {
                Stairs stairs = (Stairs) data;
                if (!stairs.isInverted() && block.getRelative(0, 1, 0).isEmpty()) {
                    sit(event.getClickedBlock(), stairs.getFacing(), event.getPlayer());
                }
            }
        }
    }

    @EventHandler
    public void onVehicleExit(final VehicleExitEvent event) {
        if (event.getVehicle() instanceof ArmorStand) {
            ((ArmorStand) event.getVehicle()).setVisible(true);
            event.getVehicle().remove();
        }
    }
}
