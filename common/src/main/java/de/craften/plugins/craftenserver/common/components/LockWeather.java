package de.craften.plugins.craftenserver.common.components;

import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * A component that locks the weather in the specified worlds.
 */
public class LockWeather extends PluginComponentBase {
    private final Set<World> worlds;

    public LockWeather(Collection<World> worlds) {
        this.worlds = new HashSet<>(worlds);
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        if (worlds.contains(event.getWorld())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onThunderChange(ThunderChangeEvent event) {
        if (worlds.contains(event.getWorld())) {
            event.setCancelled(true);
        }
    }
}
