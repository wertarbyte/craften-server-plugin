package de.craften.plugins.craftenserver.common.components.commands;

import de.craften.plugins.craftenserver.common.ServerType;

public class SpawnCommand extends ServerTeleportCommand {
    public SpawnCommand() {
        this("spawn");
    }

    public SpawnCommand(String command) {
        super(command, ServerType.LOBBY);
    }
}
