package de.craften.plugins.craftenserver.common.components;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ServerNameTitle extends PluginComponentBase implements Listener {
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        com.connorlinfoot.titleapi.TitleAPI.sendTabTitle(event.getPlayer(), getServerType().getDisplayName() + ChatColor.RESET + " @ Craften Server", null);
    }
}
