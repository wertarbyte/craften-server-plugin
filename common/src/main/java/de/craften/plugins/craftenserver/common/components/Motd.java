package de.craften.plugins.craftenserver.common.components;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.util.StringUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

/**
 * Displays MOTDs on join and provides the /motd command.
 */
public class Motd implements PluginComponent, Listener, CommandExecutor {
    private String[] motd = null;
    private CraftenServerPlugin plugin;

    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);

        PluginCommand command = plugin.getCommand("motd");
        if (command != null) {
            command.setExecutor(this);
        }

        plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("https://craften.de/api/server/motd/" + Motd.this.plugin.getType().toString());
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                        String s;
                        ArrayList<String> motd = new ArrayList<>();
                        while ((s = reader.readLine()) != null) {
                            motd.add(StringUtil.replaceColors(s.trim()));
                        }
                        Motd.this.motd = motd.toArray(new String[motd.size()]);
                    }
                } catch (IOException e) {
                    Motd.this.plugin.getLogger().warning("Could not update MOTD");
                    e.printStackTrace();
                    Motd.this.motd = null;
                }
            }
        }, 0, 20 * 60 * 15); //15 minutes
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (this.motd != null) {
            sendMotd(event.getPlayer());
        }
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (this.motd != null) {
            sendMotd(commandSender);
        }
        return true;
    }

    private void sendMotd(CommandSender sender) {
        if (this.motd != null) {
            for (String line : motd) {
                sender.sendMessage(replacePlaceholders(line, sender));
            }
        }
    }

    private String replacePlaceholders(String line, CommandSender sender) {
        return line
                .replaceAll("\\{USERNAME\\}", sender.getName())
                .replaceAll("\\{DISPLAYNAME\\}", sender instanceof Player ? ((Player) sender).getDisplayName() : sender.getName());
    }
}
