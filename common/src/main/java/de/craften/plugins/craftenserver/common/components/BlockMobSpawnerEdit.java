package de.craften.plugins.craftenserver.common.components;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Component that blocks editing mob spawners via right-click with a spawn egg for players without op.
 */
public class BlockMobSpawnerEdit extends PluginComponentBase implements Listener {
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() != null && event.getItem() != null) {
            if (!event.getPlayer().isOp() && event.getClickedBlock().getType() == Material.MOB_SPAWNER && event.getItem().getType() == Material.MONSTER_EGG) {
                event.setCancelled(true);
            }
        }
    }
}
