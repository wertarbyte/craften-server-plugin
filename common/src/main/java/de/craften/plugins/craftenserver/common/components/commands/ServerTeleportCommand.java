package de.craften.plugins.craftenserver.common.components.commands;

import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.components.PluginComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class ServerTeleportCommand implements PluginComponent, CommandExecutor {
    private final String command;
    private final ServerType server;

    private CraftenServerPlugin plugin;

    public ServerTeleportCommand(String command, ServerType server) {
        this.command = command;
        this.server = server;
    }

    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        this.plugin = plugin;
        plugin.getCommand(command).setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if (commandSender instanceof Player) {
            plugin.teleportPlayer((Player) commandSender, server);
            return true;
        }
        return false;
    }
}
