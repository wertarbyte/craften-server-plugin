package de.craften.plugins.craftenserver.common.components;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A component that displays afk players gray in the tab list and kicks afk players after some time.
 */
public class AfkPlayers extends PluginComponentBase implements Listener {
    private static final int AFK_TIME_SECONDS = 60;
    private static final int CHECK_INTERVAL_SECONDS = 30;
    private static final int KICK_AFK_TIME_MINUTES = 120;

    private Map<Player, Location> knownLocations;
    private Map<Player, Integer> afkPoints;
    private Set<Player> afkPlayers;

    @Override
    protected void onActivated() {
        knownLocations = new HashMap<>();
        afkPoints = new HashMap<>();
        afkPlayers = new HashSet<>();

        this.runTaskTimer(new Runnable() {
            @Override
            public void run() {
                for (Player player : getServer().getOnlinePlayers()) {
                    if (!afkPlayers.contains(player)) {
                        Location newLocation = player.getLocation();
                        Location knownLocation = knownLocations.get(player);
                        if (knownLocation != null && knownLocation.equals(newLocation) && afkPoints.containsKey(player)) {
                            int points = afkPoints.get(player) + 1;
                            afkPoints.put(player, points);

                            if (points >= AFK_TIME_SECONDS / CHECK_INTERVAL_SECONDS) {
                                setAfk(player, true);
                            }
                        } else {
                            knownLocations.put(player, newLocation);
                            afkPoints.put(player, 0);
                        }
                    } else {
                        int points = afkPoints.get(player) + 1;
                        afkPoints.put(player, points);

                        if (points >= 60 * KICK_AFK_TIME_MINUTES / CHECK_INTERVAL_SECONDS) {
                            player.kickPlayer("You've been afk for more than " + KICK_AFK_TIME_MINUTES + " minutes.");
                        }
                    }
                }
            }
        }, 0, 20 * CHECK_INTERVAL_SECONDS);
    }

    private void setAfk(Player player, boolean isAfk) {
        if (isAfk) {
            String name = player.getName();
            if (name.length() > 14) {
                name = name.substring(0, 14);
            }
            player.setPlayerListName(ChatColor.DARK_GRAY + name);
            afkPlayers.add(player);
        } else {
            player.setPlayerListName(player.getName());
            afkPoints.put(player, 0);
            afkPlayers.remove(player);
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        knownLocations.put(event.getPlayer(), event.getPlayer().getLocation());
        afkPoints.put(event.getPlayer(), 0);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        knownLocations.remove(event.getPlayer());
        afkPoints.remove(event.getPlayer());
        afkPlayers.remove(event.getPlayer());
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if (afkPlayers.remove(event.getPlayer())) {
            setAfk(event.getPlayer(), false);
        }
    }
}
