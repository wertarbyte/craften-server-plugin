package de.craften.plugins.craftenserver.common.components;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class TeleportOnJoin implements PluginComponent, Listener {
    private final Location location;

    public TeleportOnJoin(Location location) {
        this.location = location;
    }

    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.getPlayer().teleport(location);
    }
}
