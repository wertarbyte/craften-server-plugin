package de.craften.plugins.craftenserver.common.components;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;

/**
 * Interface for {@link PluginComponent}s that they may implement to be notified when the server plugin is disabled.
 */
public interface DisableAware {
    /**
     * Method that is  called when this component is disabled.
     *
     * @param plugin plugin
     */
    void onDisabled(CraftenServerPlugin plugin);
}
