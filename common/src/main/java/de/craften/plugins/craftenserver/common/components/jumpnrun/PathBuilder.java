package de.craften.plugins.craftenserver.common.components.jumpnrun;

import de.craften.plugins.craftenserver.common.util.region.RectangularRegion;
import de.craften.plugins.craftenserver.common.util.region.Selector;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PathBuilder {
    private final Player player;
    private final String name;
    private final File file;
    private List<Platform> platforms = new ArrayList<>();
    private Location recordSign;
    private Location recordTodaySign;

    public PathBuilder(Player player, String name, File file) {
        this.player = player;
        this.name = name;
        this.file = file;
    }

    public void addPlatform() {
        Selector.selectRegion(player).thenAccept((region) -> {
            if (region != null) {
                platforms.add(new Platform(player.getWorld().getName(),
                        new RectangularRegion(region.getXMin(), region.getZMin(),
                                region.getXMax(), region.getZMax(), region.getY() + 1)));
            }
        });
    }

    public void setRecordSign() {
        Selector.selectBlock(player, Material.SIGN, Material.WALL_SIGN, Material.SIGN_POST).thenAccept((block) -> {
            if (block != null) {
                recordSign = block.getLocation();
            }
        });
    }

    public void setRecordTodaySign() {
        Selector.selectBlock(player, Material.SIGN, Material.WALL_SIGN, Material.SIGN_POST).thenAccept((block) -> {
            if (block != null) {
                recordTodaySign = block.getLocation();
            }
        });
    }

    public Path build() {
        return new Path(
                name,
                platforms,
                recordSign,
                recordTodaySign,
                file
        );
    }
}
