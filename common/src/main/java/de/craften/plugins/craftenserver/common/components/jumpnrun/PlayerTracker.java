package de.craften.plugins.craftenserver.common.components.jumpnrun;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Date;
import java.util.UUID;

/**
 * A tracker for a {@link org.bukkit.entity.Player} that plays on a
 * Jump'n'Run {@link de.craften.plugins.craftenserver.common.components.jumpnrun.Path}.
 */
public class PlayerTracker {
    private UUID player;
    private Path path;
    private int nextPlatform;
    private long startTime = 0;
    private long endTime = 0;
    private Date endDate;

    /**
     * Creates a new tracker for the given player on the given path.
     *
     * @param player player to track
     * @param path   path the player plays on
     */
    public PlayerTracker(Player player, Path path) {
        this.player = player.getUniqueId();
        this.path = path;
    }

    /**
     * Gets the player this tracker tracks.
     *
     * @return player this tracker tracks
     */
    public Player getPlayer() {
        return Bukkit.getPlayer(player);
    }

    /**
     * Gets the path this tracker tracks a player on.
     *
     * @return path this tracker tracks a player on
     */
    public Path getPath() {
        return path;
    }

    /**
     * Starts the stopwatch.
     */
    public void start() {
        startTime = System.currentTimeMillis();
        nextPlatform = 1;
    }

    /**
     * Stops the stopwatch.
     */
    public void stop() {
        endTime = System.currentTimeMillis();
        endDate = new Date();
    }

    /**
     * Gets the score the player achieved.
     *
     * @return score the player achieved
     * @throws java.lang.IllegalStateException if the time was not yet tracked
     */
    public Score getScore() {
        if (startTime == 0 || endTime == 0) {
            throw new IllegalStateException("No time tracked, yet.");
        }
        return new Score(getPlayer().getName(), path, endDate, endTime - startTime);
    }

    /**
     * Checks if this tracker had been started.
     *
     * @return true if this tracker had been started, false if not
     */
    public boolean isStarted() {
        return startTime > 0;
    }

    /**
     * Updates the next platform the tracked player has to reach so that the time is valid (shortcuts are not allowed).
     */
    public void updatePlatform() {
        Platform next = path.getPlatform(nextPlatform);
        if (next != null && next.isInside(getPlayer().getLocation())) {
            nextPlatform++;
        }
    }

    /**
     * Checks if the player may be at the last platform. For this check to be true, the player needs to have reached
     * all platforms of this path.
     *
     * @return true if the player may be at the last platform, i.e. didn't use any shortcuts
     */
    public boolean mayBeAtEnd() {
        return nextPlatform >= path.getPlatformCount();
    }
}
