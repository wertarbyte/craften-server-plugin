package de.craften.plugins.craftenserver.common.components.jumpnrun;

import de.craften.plugins.craftenserver.common.components.PluginComponentBase;
import de.craften.plugins.craftenserver.common.util.StringUtil;
import de.craften.plugins.craftenserver.common.util.YamlFileFilter;
import de.craften.plugins.mcguilib.text.TextBuilder;
import me.mickyjou.plugins.gems.api.GemProvider;
import me.mickyjou.plugins.gems.gemextras.abilitymanager.AbilityManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

/**
 * A component for Jump'n'Run games on multiple paths with record-signs.
 */
public class JumpAndRunComponent extends PluginComponentBase implements Listener, CommandExecutor {
    /**
     * The minimum duration of a Jump'n'Run. If a player is faster than this time, the time is considered invalid.
     */
    private static final long MIN_SCORE_TIME_MS = 2000;

    /**
     * The tracker for every player that currently plays a Jump'n'Run.
     */
    private Map<UUID, PlayerTracker> jumpers = new HashMap<>();

    /**
     * The last-started path for each player.
     */
    private Map<UUID, Path> lastPaths = new HashMap<>();

    /**
     * All available Jump'n'Run paths.
     */
    private List<Path> paths;

    /**
     * The name of the 'stick of return' that is used to return to the start. May contain format characters.
     */
    private static final String STICK_OF_RETURN = ChatColor.DARK_RED + "Stick of Return";
    private HashMap<UUID, PathBuilder> pathBuilders = new HashMap<>();

    protected void onActivated() {
        loadPaths();
        registerCommand("jnr", this);

        runTaskTimer(new Runnable() {
            @Override
            public void run() {
                for (Path path : paths) {
                    if (path.getRecordToday() != null) {
                        Date recordTodayDate = path.getRecordToday().getDate();
                        Calendar c1 = Calendar.getInstance();
                        c1.setTime(recordTodayDate);
                        if (c1.get(Calendar.DAY_OF_MONTH) != Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) {
                            //'today' record date is not today => reset record
                            path.resetRecordToday();
                        }
                    }
                }
            }
        }, 0, 20 * 60 * 15);
    }

    private void loadPaths() {
        paths = new ArrayList<>();

        File pathsDirectory = new File(getDataFolder(), "jumpnrun");

        if (pathsDirectory.exists() && pathsDirectory.isDirectory()) {
            for (File pathFile : pathsDirectory.listFiles(new YamlFileFilter())) {
                paths.add(Path.load(pathFile));
            }
        } else {
            getLogger().warning("Jump'n'Run directory " + pathsDirectory.getAbsolutePath() + " not found");
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        PlayerTracker tracker = jumpers.get(event.getPlayer().getUniqueId());
        if (tracker != null) {
            if (tracker.isStarted()) {
                tracker.updatePlatform();
                if (tracker.getPath().isAtEnd(event.getPlayer())) {
                    //player is already tracked and reached the end of the path
                    tracker.stop();
                    if (!tracker.mayBeAtEnd()) {
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.EXPLODE, 10, 1);
                        TextBuilder.create("No shortcuts!").red().sendTo(event.getPlayer());
                    } else if (tracker.getScore().getTime() < MIN_SCORE_TIME_MS) {
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.EXPLODE, 10, 1);
                        TextBuilder.create("Invalid time!").red().sendTo(event.getPlayer());
                    } else {
                        TextBuilder.create()
                                .append("You finished ").darkGreen()
                                .append(tracker.getPath().getName()).yellow()
                                .append(". Time: ").darkGreen()
                                .append(tracker.getScore().getFormattedTime()).yellow()
                                .sendTo(event.getPlayer());
                        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.LEVEL_UP, 10, 1);
                        onPathFinished(tracker);
                    }
                    jumpers.remove(event.getPlayer().getUniqueId());
                    AbilityManager.getInstance().ifPresent(abilityManager -> abilityManager.unpauseAllAbilities(event.getPlayer()));
                } else {
                    //player may have gone to the start of another path
                    for (Path path : paths) {
                        if (path.isAtStart(event.getPlayer())) {
                            jumpers.put(event.getPlayer().getUniqueId(), new PlayerTracker(event.getPlayer(), path));
                            break;
                        }
                    }
                }
            } else if (!tracker.getPath().isAtStart(event.getPlayer())) {
                AbilityManager.getInstance().ifPresent(abilityManager -> abilityManager.pauseAllAbilities(event.getPlayer()));

                ItemStack stickOfReturn = new ItemStack(Material.STICK, 1);
                ItemMeta meta = stickOfReturn.getItemMeta();
                meta.setDisplayName(STICK_OF_RETURN);
                stickOfReturn.setItemMeta(meta);
                event.getPlayer().getInventory().setItemInHand(stickOfReturn);

                TextBuilder.create()
                        .append("You started ").darkGreen()
                        .append(tracker.getPath().getName()).yellow()
                        .append(". Have fun! Use the ").darkGreen()
                        .append("Stick of Return").yellow()
                        .append(" to get back to the start.").darkGreen()
                        .sendTo(event.getPlayer());
                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ORB_PICKUP, 5, 1);
                tracker.start();

                lastPaths.put(event.getPlayer().getUniqueId(), tracker.getPath());
            }
        } else {
            //player not tracked but may have gone to the start of a path
            for (Path path : paths) {
                if (path.isAtStart(event.getPlayer())) {
                    jumpers.put(event.getPlayer().getUniqueId(), new PlayerTracker(event.getPlayer(), path));
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Action action = event.getAction();
        if (action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK ||
                action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
            ItemStack item = event.getItem();
            if (item != null && item.hasItemMeta() && STICK_OF_RETURN.equals(item.getItemMeta().getDisplayName())) {
                Player player = event.getPlayer();
                PlayerTracker tracker = jumpers.get(player.getUniqueId());
                if (tracker != null && tracker.isStarted()) {
                    tracker.stop();
                    tracker.getPath().getStart().teleport(player);
                } else {
                    Path path = lastPaths.get(player.getUniqueId());
                    if (path != null) {
                        path.getStart().teleport(player);
                    }
                }
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        jumpers.remove(event.getPlayer().getUniqueId()); //prevent players from cheating
        AbilityManager.getInstance().ifPresent(abilityManager -> abilityManager.unpauseAllAbilities(event.getPlayer()));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        jumpers.remove(event.getPlayer().getUniqueId());
        lastPaths.remove(event.getPlayer().getUniqueId());
        pathBuilders.remove(event.getPlayer());
    }

    /**
     * Updates the path's records and broadcasts messages when a player finishes a track.
     *
     * @param tracker tracker that tracked the player
     */
    private void onPathFinished(PlayerTracker tracker) {
        Path path = tracker.getPath();
        Score score = tracker.getScore();

        if (path.getRecord() == null || score.getTime() < path.getRecord().getTime()) {
            path.setRecord(score);
            path.setRecordToday(score);
            TextBuilder.create()
                    .append(score.getNickname()).yellow()
                    .append(" set a ").darkGreen()
                    .append("new record").yellow()
                    .append(" on ").darkGreen()
                    .append(path.getName()).yellow()
                    .append("! Time: ").darkGreen()
                    .append(score.getFormattedTime()).yellow()
                    .broadcast();
        } else if (path.getRecordToday() == null || score.getTime() < path.getRecordToday().getTime()) {
            path.setRecordToday(score);
            TextBuilder.create()
                    .append(score.getNickname()).yellow()
                    .append(" set a ").darkGreen()
                    .append("new day record").yellow()
                    .append(" on ").darkGreen()
                    .append(path.getName()).yellow()
                    .append("! Time: ").darkGreen()
                    .append(score.getFormattedTime()).yellow()
                    .broadcast();
        }

        try {
            path.saveRecords();
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Could not save path", e);
        }

        if (path.getGemReward() > 0 && Bukkit.getServicesManager().isProvidedFor(GemProvider.class)) {
            Bukkit.getServicesManager().getRegistration(GemProvider.class).getProvider().addGems(tracker.getPlayer(), path.getGemReward());
            TextBuilder.create("You won ")
                    .append(path.getGemReward() + " Gems").gold()
                    .append("!")
                    .sendTo(tracker.getPlayer());
        }
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        switch (args[0]) {
            case "reset":
                Path path = paths.get(Integer.parseInt(args[1]));
                path.resetRecordToday();
                commandSender.sendMessage("[JnR] Reset today's record of " + path.getName());
                if (args.length > 2 && args[2].equals("hard")) {
                    path.resetRecord();
                    commandSender.sendMessage("[JnR] Reset record of " + path.getName());
                }
                return true;
            case "list":
                for (int i = 0; i < paths.size(); i++) {
                    commandSender.sendMessage("[JnR] " + i + ": " + paths.get(i).getName());
                }
                return true;
            case "reload":
                loadPaths();
                commandSender.sendMessage("[JnR] Reloaded");
                return true;
            case "create":
                if (commandSender instanceof Player && args.length >= 2) {
                    PathBuilder pathBuilder = pathBuilders.get(((Player) commandSender).getUniqueId());
                    if (pathBuilder == null) {
                        File pathFile = new File(new File(getDataFolder(), "jumpnrun"), "jnr_" + System.currentTimeMillis() + ".yml");
                        pathBuilder = new PathBuilder((Player) commandSender, StringUtil.join(Arrays.copyOfRange(args, 1, args.length), " "), pathFile);
                        pathBuilders.put(((Player) commandSender).getUniqueId(), pathBuilder);
                        commandSender.sendMessage("[JnR] Jump'n'Run builder ready.");
                        commandSender.sendMessage("[JnR] Create a checkpoint with " + ChatColor.YELLOW + "/jnr create cp");
                        commandSender.sendMessage("[JnR] Select a record sign with " + ChatColor.YELLOW + "/jnr create record" + ChatColor.RESET + " or " + ChatColor.YELLOW + "/jnr create recordToday");
                        commandSender.sendMessage("[JnR] Save the JnR with " + ChatColor.YELLOW + "/jnr create done");
                        return true;
                    }
                    switch (args[1].toLowerCase()) {
                        case "cp":
                            pathBuilder.addPlatform();
                            break;
                        case "record":
                            pathBuilder.setRecordSign();
                            break;
                        case "recordtoday":
                            pathBuilder.setRecordTodaySign();
                            break;
                        case "done":
                            try {
                                pathBuilder.build().save();
                                pathBuilders.remove(((Player) commandSender).getUniqueId());
                            } catch (IOException e) {
                                commandSender.sendMessage(ChatColor.RED + "[JnR] The Jump'n'Run could not be saved.");
                            }
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
                return false;
        }
        return false;
    }
}
