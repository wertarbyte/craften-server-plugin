package de.craften.plugins.craftenserver.common.components;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * A component that adds speed boost and jump fields by placing pressure plates on special blocks.
 */
public class BoostFields extends PluginComponentBase implements Listener {
    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player p = event.getPlayer();
        Block block = p.getLocation().getBlock();
        if (block.getType() == Material.STONE_PLATE || block.getType() == Material.WOOD_PLATE) {
            Material below = p.getLocation().subtract(0, 1, 0).getBlock().getType();

            if (below == Material.REDSTONE_BLOCK) {
                p.setVelocity(p.getLocation().getDirection().multiply(7).setY(1));
                p.playSound(p.getLocation(), Sound.ENDERDRAGON_WINGS, 3, 2);
            } else if (below == Material.EMERALD_BLOCK) {
                p.setVelocity(p.getLocation().getDirection().multiply(12).setY(2.5));
                p.playSound(p.getLocation(), Sound.ENDERDRAGON_WINGS, 3, 2);
            }
        }
    }
}
