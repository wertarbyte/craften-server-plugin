package de.craften.plugins.craftenserver.common.util.commands;

import org.bukkit.command.CommandSender;

import java.util.List;

/**
 * A handler for a command.
 */
public interface CommandHandler {
    boolean onCommand(CommandSender sender, String command, List<String> args);
}
