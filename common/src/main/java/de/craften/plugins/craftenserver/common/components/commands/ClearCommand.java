package de.craften.plugins.craftenserver.common.components.commands;

import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.components.PluginComponent;
import de.craften.plugins.craftenserver.common.util.StringUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClearCommand implements PluginComponent, CommandExecutor {
    private CraftenServerPlugin plugin;

    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        this.plugin = plugin;
        plugin.getCommand("clear").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if (commandSender.hasPermission("csp.clear")) {
            if (args.length >= 1) {
                plugin.getServer().broadcastMessage(ChatColor.RED + "You'll now be teleported to the lobby. Reason: "
                        + ChatColor.GOLD + StringUtil.join(args, " "));
            } else {
                plugin.getServer().broadcastMessage(ChatColor.RED + "You'll now be teleported to the lobby.");
            }

            for (Player p : plugin.getServer().getOnlinePlayers())
                plugin.teleportPlayer(p, ServerType.LOBBY);
            return true;
        } else {
            return false;
        }
    }
}
