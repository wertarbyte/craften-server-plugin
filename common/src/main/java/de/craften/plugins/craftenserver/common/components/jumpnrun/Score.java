package de.craften.plugins.craftenserver.common.components.jumpnrun;

import java.util.Date;

/**
 * A score on a Jump'n'Run path.
 */
public class Score {
    private String nickname;
    private Path path;
    private Date date;
    private long milliseconds;

    /**
     * Creates a new score.
     *
     * @param nickname     nickname of the player
     * @param path         path this score was achieved on
     * @param date         date this score was got
     * @param milliseconds time in milliseconds
     */
    public Score(String nickname, Path path, Date date, long milliseconds) {
        this.nickname = nickname;
        this.path = path;
        this.date = date;
        this.milliseconds = milliseconds;
    }

    /**
     * Gets the nickname of the player that got this score.
     *
     * @return nickname of the player that got this score
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Gets the path this score was achieved on.
     *
     * @return path this score was achieved on
     */
    public Path getPath() {
        return path;
    }

    /**
     * Gets the date when this score was achieved.
     *
     * @return date when this score was achieved
     */
    public Date getDate() {
        return date;
    }

    /**
     * Gets the time in milliseconds.
     *
     * @return time in milliseconds
     */
    public long getTime() {
        return milliseconds;
    }

    /**
     * Gets the time as formatted string in the format m:ss.ff where mm is the minutes, ss seconds and ff hundredths
     * of seconds.
     *
     * @return time as formatted string
     */
    public String getFormattedTime() {
        int hundredths = (int) ((milliseconds / 10) % 100);
        int seconds = (int) ((milliseconds / 1000) % 60);
        int minutes = (int) ((milliseconds / 1000) / 60);

        return String.format("%d:%02d.%02d", minutes, seconds, hundredths);
    }
}
