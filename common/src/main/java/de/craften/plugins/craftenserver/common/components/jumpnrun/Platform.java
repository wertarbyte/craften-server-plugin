package de.craften.plugins.craftenserver.common.components.jumpnrun;

import de.craften.plugins.craftenserver.common.util.region.RectangularRegion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * A region at a specific position in a world.
 */
public class Platform {
    private String world;
    private RectangularRegion region;

    /**
     * Creates a new platform.
     *
     * @param world  world of the platform
     * @param region two-dimensional region of the platform
     */
    public Platform(String world, RectangularRegion region) {
        this.world = world;
        this.region = region;
    }

    /**
     * Checks if the given location is on this platform.
     *
     * @param location location
     * @return true if the given location is on this platform, false if not
     */
    public boolean isInside(Location location) {
        return location.getWorld().getName().equals(world) && region.contains(location);
    }

    /**
     * Gets the world this platform is located in.
     *
     * @return the world this platform is located in
     */
    public World getWorld() {
        return Bukkit.getWorld(world);
    }

    /**
     * Teleports the given player to this platform.
     *
     * @param player player to teleport
     */
    public void teleport(Player player) {
        double centerX = (region.getXMin() + region.getXMax()) / 2.0 + 0.5;
        double centerZ = (region.getZMin() + region.getZMax()) / 2.0 + 0.5;

        player.teleport(new Location(getWorld(), centerX, region.getY(), centerZ), PlayerTeleportEvent.TeleportCause.PLUGIN);
    }

    /**
     * Gets this platform's region.
     *
     * @return
     */
    public RectangularRegion getRegion() {
        return region;
    }
}
