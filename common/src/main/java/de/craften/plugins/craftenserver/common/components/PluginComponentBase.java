package de.craften.plugins.craftenserver.common.components;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.util.commands.CommandHandler;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.permissions.Permissible;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 * A base for plugin components that makes many things easier.
 * <p>
 * If the extending class implements {@link org.bukkit.event.Listener}, it is automatically registered.
 */
public abstract class PluginComponentBase implements PluginComponent, DisableAware {
    private CraftenServerPlugin plugin;

    @Override
    public final void activateFor(CraftenServerPlugin plugin) {
        this.plugin = plugin;

        if (this instanceof Listener) {
            registerEvents((Listener) this);
        }

        onActivated();
    }

    @Override
    public final void onDisabled(CraftenServerPlugin plugin) {
        onDisabled();
    }

    /**
     * Registers the given listener.
     *
     * @param listener listener to register
     */
    public final void registerEvents(Listener listener) {
        plugin.getServer().getPluginManager().registerEvents(listener, plugin);
    }

    /**
     * Gets a logger for this component.
     *
     * @return logger for this component
     */
    public final Logger getLogger() {
        return plugin.getLogger();
    }

    /**
     * Registers an executor for a command. The command must be declared in the plugin.yml in order to work.
     *
     * @param command  command to register
     * @param executor executor for that command
     * @deprecated use {@link #registerCommand(String, CommandHandler)} or {@link #registerCommand(String[], CommandHandler)} instead
     */
    @Deprecated
    public final void registerCommand(String command, CommandExecutor executor) {
        PluginCommand cmd = plugin.getCommand(command);
        if (cmd != null) {
            cmd.setExecutor(executor);
        } else {
            getLogger().warning("Command '" + command + "' is not declared in the plugin.yml so it is not available.");
        }
    }

    /**
     * Registers a handler for a command. The command does not need to be declared in the plugin.yml in order to work.
     *
     * @param command command to register
     * @param handler handler for that command
     */
    public final void registerCommand(String command, CommandHandler handler) {
        plugin.getCustomCommands().registerCommand(command, handler);
    }

    /**
     * Registers a handler for a (sub-) command. The root command does not need to be declared in the plugin.yml in
     * order to work.
     *
     * @param command (sub-) command to register
     * @param handler handler for that (sub-) command
     */
    public final void registerCommand(String[] command, CommandHandler handler) {
        plugin.getCustomCommands().registerCommand(command, handler);
    }

    private static CommandMap getCommandMap() {
        try {
            Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);
            return (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new UnsupportedOperationException("Command could not be registered", e);
        }
    }

    /**
     * Gets the plugin configuration
     *
     * @return plugin configuration
     */
    public final Configuration getConfig() {
        return plugin.getConfig();
    }

    /**
     * Saves the plugin configuration.
     */
    public final void saveConfig() {
        plugin.saveConfig();
    }

    /**
     * Gets the plugin's data folder.
     *
     * @return the plugin's data folder
     */
    public final File getDataFolder() {
        return plugin.getDataFolder();
    }

    /**
     * Gets the server.
     *
     * @return currently running server
     */
    public final Server getServer() {
        return plugin.getServer();
    }

    /**
     * Teleports the given player to the given server.
     *
     * @param player player to teleport
     * @param server server to teleport the player to
     */
    public final void teleportPlayer(Player player, ServerType server) {
        plugin.teleportPlayer(player, server);
    }

    /**
     * Returns a task that will repeatedly run until cancelled, starting after the specified number of server ticks.
     *
     * @param task   the task to be run
     * @param delay  the ticks to wait before running the task
     * @param period the ticks to wait between runs
     * @return a BukkitTask that contains the id number
     */
    public final BukkitTask runTaskTimer(Runnable task, long delay, long period) {
        return getServer().getScheduler().runTaskTimer(plugin, task, delay, period);
    }

    /**
     * Returns a task that will repeatedly run asynchronously until cancelled, starting after the specified number of
     * server ticks.
     *
     * @param task   the task to be run
     * @param delay  the ticks to wait before running the task
     * @param period the ticks to wait between runs
     * @return a BukkitTask that contains the id number
     */
    public final BukkitTask runTaskTimerAsynchronously(Runnable task, long delay, long period) {
        return getServer().getScheduler().runTaskTimerAsynchronously(plugin, task, delay, period);
    }

    /**
     * Returns a task that will run once after the specified number of server ticks.
     *
     * @param task  the task to be run
     * @param delay the ticks to wait before running the task
     * @return a BukkitTask that contains the id number
     */
    public final BukkitTask runTaskLater(Runnable task, long delay) {
        return getServer().getScheduler().runTaskLater(plugin, task, delay);
    }

    /**
     * Gives the given permission with the given value to the given permissible object (i.e. a player) for 2 seconds.
     *
     * @param permissible permissible object, i.e. a player
     * @param name        name of the permission
     * @param value       value
     */
    public final void addTemporaryPermission(Permissible permissible, String name, boolean value) {
        permissible.addAttachment(plugin, name, value, 40);
    }

    /**
     * Get the type of this server.
     *
     * @return the type of this server
     */
    public ServerType getServerType() {
        return plugin.getType();
    }

    /**
     * Method that is called after this component was activated.
     */
    protected void onActivated() {
    }

    /**
     * Method that is  called when this component is disabled.
     */
    protected void onDisabled() {
    }
}
