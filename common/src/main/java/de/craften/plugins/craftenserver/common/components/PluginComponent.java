package de.craften.plugins.craftenserver.common.components;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;

public interface PluginComponent {
    /**
     * Method that is called after this component was activated.
     * @param plugin plugin
     */
    void activateFor(CraftenServerPlugin plugin);
}
