package de.craften.plugins.craftenserver.common;

import org.bukkit.ChatColor;

public enum ServerType {
    LOBBY("lobby", ChatColor.GOLD + "Lobby"),
    SURVIVAL("craften", ChatColor.BLUE + "Survival"),
    PAINTBALL("paintball", ChatColor.GOLD + "Paintball"),
    MINIGAMES("minigames", ChatColor.RED + "Minigames"),
    SKYBLOCK("skyblock", ChatColor.DARK_PURPLE + "Skyblock"),
    ARCHITECTS("architects", ChatColor.GOLD + "Architects"),
    CREATIVE("creative", ChatColor.YELLOW + "Creative"),
    GOOTHA("gootha", ChatColor.GREEN + "Gootha");

    private final String name;
    private final String displayName;

    ServerType(String name, String displayName) {
        this.name = name;
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return name();
    }
}
