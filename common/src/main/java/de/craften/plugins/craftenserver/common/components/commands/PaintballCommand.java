package de.craften.plugins.craftenserver.common.components.commands;

import de.craften.plugins.craftenserver.common.ServerType;

public class PaintballCommand extends ServerTeleportCommand {
    public PaintballCommand() {
        super("paintball", ServerType.PAINTBALL);
    }
}
