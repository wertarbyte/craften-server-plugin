package de.craften.plugins.craftenserver.common.util.region;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Provides interactive selectors for regions and blocks.
 * You need to enable it before using with {@link #register(Plugin)}.
 */
public class Selector implements Listener {
    private static Selector selector;
    private Map<Player, SelectionState> selections = new HashMap<>();

    private Selector() {
    }

    private void cancelSelection(Player player) {
        SelectionState selection = selections.remove(player);
        if (selection != null) {
            selection.cancel();
        }
    }

    private CompletableFuture<RectangularRegion> startSelectRegion(Player player) {
        cancelSelection(player);

        RegionSelectionState selectionState = new RegionSelectionState(player);
        selections.put(player, selectionState);
        player.sendMessage(ChatColor.YELLOW + "Please right-click on two corner blocks to select a region.");
        return selectionState.result;
    }

    private CompletableFuture<Block> startSelectBlock(Player player, Material... allowedMaterials) {
        cancelSelection(player);

        BlockSelectionState selectionState = new BlockSelectionState(player, allowedMaterials);
        selections.put(player, selectionState);
        player.sendMessage(ChatColor.YELLOW + "Please select a block by right-clicking on it. Allowed block types: " + Arrays.toString(allowedMaterials));
        return selectionState.result;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        cancelSelection(event.getPlayer());
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerClickBlock(PlayerInteractEvent event) {
        SelectionState selection = selections.get(event.getPlayer());
        if (selection != null) {
            selection.handleInteractionEvent(event);
            event.setCancelled(true);
        }
    }

    /**
     * Let a player select a region.
     *
     * @param player player
     * @return future that will be completed with the selected region
     */
    public static CompletableFuture<RectangularRegion> selectRegion(Player player) {
        if (selector == null) {
            throw new IllegalStateException("Selector was not registered");
        }
        return selector.startSelectRegion(player);
    }

    /**
     * Let a player select a block.
     *
     * @param player player
     * @return future that will be completed with the selected region
     */
    public static CompletionStage<Block> selectBlock(Player player, Material... allowedMaterials) {
        if (selector == null) {
            throw new IllegalStateException("Selector was not registered");
        }
        return selector.startSelectBlock(player, allowedMaterials);
    }

    /**
     * Register the region selector using the given plugin.
     *
     * @param plugin plugin
     */
    public static void register(Plugin plugin) {
        selector = new Selector();
        plugin.getServer().getPluginManager().registerEvents(selector, plugin);
    }

    private interface SelectionState {
        void handleInteractionEvent(PlayerInteractEvent event);

        void cancel();
    }

    private class RegionSelectionState implements SelectionState {
        private final CompletableFuture<RectangularRegion> result = new CompletableFuture<>();
        private final Player player;
        private PointXZ p1;
        private PointXZ p2;
        private int y;

        public RegionSelectionState(Player player) {
            this.player = player;
        }

        public void selectPoint(Location location) {
            if (p1 == null) {
                p1 = new PointXZ(location.getBlockX(), location.getBlockZ());
                y = location.getBlockY();
                player.sendMessage(ChatColor.YELLOW + "Now right-click on another block to select the second corner.");
            } else if (p2 == null) {
                p2 = new PointXZ(location.getBlockX(), location.getBlockZ());

                selections.remove(player);
                player.sendMessage(ChatColor.YELLOW + "Region selected.");
                result.complete(new RectangularRegion(p1, p2, y));
            }
        }

        @Override
        public void handleInteractionEvent(PlayerInteractEvent event) {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                selectPoint(event.getClickedBlock().getLocation());
            }
        }

        @Override
        public void cancel() {
            selections.remove(player);
            result.complete(null);
        }
    }

    private class BlockSelectionState implements SelectionState {
        private final CompletableFuture<Block> result = new CompletableFuture<>();
        private final Player player;
        private final Material[] allowedMaterials;

        private BlockSelectionState(Player player, Material[] allowedMaterials) {
            this.player = player;
            this.allowedMaterials = allowedMaterials;
        }

        private void selectBlock(Block clickedBlock) {
            for (Material material : allowedMaterials) {
                if (material == clickedBlock.getType()) {
                    selections.remove(player);
                    player.sendMessage(ChatColor.YELLOW + "Block selected.");
                    result.complete(clickedBlock);
                    return;
                }
            }
            player.sendMessage(ChatColor.YELLOW + "Please select one of the following blocks types: " + Arrays.toString(allowedMaterials));
        }

        @Override
        public void handleInteractionEvent(PlayerInteractEvent event) {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                selectBlock(event.getClickedBlock());
            }
        }

        @Override
        public void cancel() {
            selections.remove(player);
            result.complete(null);
        }
    }
}
