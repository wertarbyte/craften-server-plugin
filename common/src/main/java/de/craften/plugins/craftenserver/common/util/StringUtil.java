package de.craften.plugins.craftenserver.common.util;


public class StringUtil {
    public static String join(String[] input, String delimiter) {
        StringBuilder sb = new StringBuilder();
        for (String value : input) {
            sb.append(value);
            sb.append(delimiter);
        }
        int length = sb.length();
        if (length > 0) {
            // Remove the extra delimiter
            sb.setLength(length - delimiter.length());
        }
        return sb.toString();
    }

    /**
     * Replaces color and formatting code of the form '&#' where # is 0..1, a..f or k,l,m,n,o.r with the correct
     * Minecraft formatting code.
     *
     * @param message original string to replace formatting codes in
     * @return string with Minecraft formatting codes
     */
    public static String replaceColors(String message) {
        return message.replaceAll("(?i)&([a-f0-9klmnor])", "\u00A7$1");
    }
}
