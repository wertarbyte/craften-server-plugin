package de.craften.plugins.craftenserver.common.components.commands;

import de.craften.plugins.craftenserver.common.ServerType;

public class SkyblockCommand extends ServerTeleportCommand {
    public SkyblockCommand() {
        super("skyblock", ServerType.SKYBLOCK);
    }
}
