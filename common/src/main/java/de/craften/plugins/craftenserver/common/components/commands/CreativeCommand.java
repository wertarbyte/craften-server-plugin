package de.craften.plugins.craftenserver.common.components.commands;

import de.craften.plugins.craftenserver.common.ServerType;

/**
 * Adds a /creative command that teleports players to the creative server.
 */
public class CreativeCommand extends ServerTeleportCommand {
    public CreativeCommand() {
        super("creative", ServerType.CREATIVE);
    }
}
