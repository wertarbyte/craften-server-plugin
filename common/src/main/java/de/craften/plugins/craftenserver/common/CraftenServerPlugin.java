package de.craften.plugins.craftenserver.common;

import de.craften.plugins.craftenserver.common.components.DisableAware;
import de.craften.plugins.craftenserver.common.components.PluginComponent;
import de.craften.plugins.craftenserver.common.components.ServerNameTitle;
import de.craften.plugins.craftenserver.common.util.BungeeApi;
import de.craften.plugins.craftenserver.common.util.commands.CustomCommands;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public abstract class CraftenServerPlugin extends JavaPlugin {
    private List<PluginComponent> components = new ArrayList<>();
    private CustomCommands customCommands;

    protected void addComponents(PluginComponent... components) {
        customCommands = new CustomCommands();

        for (PluginComponent c : components) {
            this.components.add(c);
            c.activateFor(this);
        }
    }

    public void teleportPlayer(Player player, ServerType server) {
        BungeeApi.teleportPlayer(this, player, server.toString());
    }

    public CustomCommands getCustomCommands() {
        return this.customCommands;
    }

    @Override
    public void onEnable() {
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord"); //required to use Bungee API

        addComponents(new ServerNameTitle());
    }

    @Override
    public void onDisable() {
        for (PluginComponent c : components) {
            if (c instanceof DisableAware) {
                ((DisableAware) c).onDisabled(this);
            }
        }

        customCommands.removeAll();
    }

    /**
     * Gets the server type.
     *
     * @return server type
     */
    public abstract ServerType getType();
}
