package de.craften.plugins.craften.minigames;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.components.DisableJoinQuitMessages;
import de.craften.plugins.craftenserver.common.components.MaintenanceMode;
import de.craften.plugins.craftenserver.common.components.Motd;
import de.craften.plugins.craftenserver.common.components.commands.ClearCommand;
import de.craften.plugins.craftenserver.common.components.commands.CreativeCommand;
import de.craften.plugins.craftenserver.common.components.commands.SkyblockCommand;
import de.craften.plugins.craftenserver.common.components.commands.SpawnCommand;

public class Plugin extends CraftenServerPlugin {
    @Override
    public void onEnable() {
        super.onEnable();

        addComponents(
                new ClearCommand(),
                new SpawnCommand(),
                new SkyblockCommand(),
                new DisableJoinQuitMessages(),
                new CreativeCommand(),
                new MaintenanceMode(),
                new Motd()
        );
    }

    @Override
    public ServerType getType() {
        return ServerType.MINIGAMES;
    }
}
