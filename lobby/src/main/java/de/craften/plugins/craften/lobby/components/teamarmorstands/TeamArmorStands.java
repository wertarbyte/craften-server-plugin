package de.craften.plugins.craften.lobby.components.teamarmorstands;

import de.craften.plugins.craftenserver.common.components.PluginComponentBase;
import org.bukkit.*;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.event.Listener;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A component that adds armorstands for team members.
 */
public class TeamArmorStands extends PluginComponentBase implements Listener {
    private List<TeamArmorStand> armorStands;

    @Override
    protected void onActivated() {
        final boolean[] left = {true};
        double y = 73;
        final double[] z = {-441.5 - 3};

        armorStands = getConfig().getMapList("teamArmorStands").stream()
                .map((map) -> {
                    left[0] = !left[0];
                    if (!left[0]) {
                        z[0] += 3;
                    }
                    Location location = new Location(
                            Bukkit.getWorld("world"),
                            left[0] ? 550.5 : 544.5,
                            y,
                            z[0],
                            left[0] ? 90 : -90,
                            0.0f
                    );
                    location.getBlock().setType(Material.BARRIER);
                    location.getBlock().getRelative(BlockFace.UP).setType(Material.BARRIER);
                    location.getBlock().getRelative(left[0] ? BlockFace.WEST : BlockFace.EAST).setType(Material.WALL_SIGN);
                    Sign sign = (Sign) location.getBlock().getRelative(left[0] ? BlockFace.WEST : BlockFace.EAST).getState();
                    org.bukkit.material.Sign signData = (org.bukkit.material.Sign) sign.getData();
                    signData.setFacingDirection(left[0] ? BlockFace.WEST : BlockFace.EAST);
                    sign.setData(signData);
                    sign.setLine(0, "");
                    sign.setLine(1, ChatColor.BOLD + map.get("name").toString());
                    if (map.containsKey("role")) {
                        sign.setLine(2, map.get("role").toString());
                    } else {
                        sign.setLine(2, "");
                    }
                    sign.update(true);
                    return TeamArmorStand.spawn(
                            map.get("name").toString(),
                            hexToColor(map.get("color").toString()),
                            location);
                })
                .collect(Collectors.toList());

        runTaskTimer(() -> armorStands.forEach(TeamArmorStand::update), 1, 1);
    }

    @Override
    protected void onDisabled() {
        armorStands.forEach(TeamArmorStand::remove);
    }

    public static Color hexToColor(String colorStr) {
        return Color.fromRGB(
                Integer.valueOf(colorStr.substring(1, 3), 16),
                Integer.valueOf(colorStr.substring(3, 5), 16),
                Integer.valueOf(colorStr.substring(5, 7), 16));
    }
}
