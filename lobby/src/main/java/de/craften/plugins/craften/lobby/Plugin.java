package de.craften.plugins.craften.lobby;

import de.craften.plugins.craften.lobby.components.NoFallDamage;
import de.craften.plugins.craften.lobby.components.NoMobDamage;
import de.craften.plugins.craften.lobby.components.WelcomeTitle;
import de.craften.plugins.craften.lobby.components.shortcutinventory.ShortcutInventories;
import de.craften.plugins.craften.lobby.components.teamarmorstands.TeamArmorStands;
import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.components.*;
import de.craften.plugins.craftenserver.common.components.commands.CreativeCommand;
import de.craften.plugins.craftenserver.common.components.commands.PaintballCommand;
import de.craften.plugins.craftenserver.common.components.commands.SkyblockCommand;
import de.craften.plugins.craftenserver.common.components.jumpnrun.JumpAndRunComponent;
import de.craften.plugins.craftenserver.common.util.region.Selector;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Plugin extends CraftenServerPlugin {
    @Override
    public void onEnable() {
        super.onEnable();
        Selector.register(this);

        addComponents(
                new PaintballCommand(),
                new SkyblockCommand(),
                new TeleportOnJoin(Bukkit.getWorld("world").getSpawnLocation()),
                new CreativeCommand(),
                new Motd(),
                new NoMobDamage(),
                new DisableJoinQuitMessages(),
                new JumpAndRunComponent(),
                new AfkPlayers(),
                new WelcomeTitle(),
                new PlayerSeats(),
                new BoostFields(),
                new NoFallDamage(),
                new ShortcutInventories(),
                new LockWeather(Bukkit.getWorlds()),
                new TeamArmorStands()
        );
    }

    @Override
    public ServerType getType() {
        return ServerType.LOBBY;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("spawn")) {
            Player player = (Player) sender;
            player.teleport(player.getWorld().getSpawnLocation());
            return true;
        } else if (command.getName().equals("setspawn") && sender instanceof Player && sender.isOp()) {
            Player player = (Player) sender;
            Location l = player.getLocation();
            player.getWorld().setSpawnLocation(l.getBlockX(), l.getBlockY(), l.getBlockZ());
            return true;
        }
        return false;
    }
}
