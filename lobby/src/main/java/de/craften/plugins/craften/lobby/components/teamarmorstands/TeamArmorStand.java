package de.craften.plugins.craften.lobby.components.teamarmorstands;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.EulerAngle;

import java.util.Collection;

/**
 * An armor stand that moves its head to the nearest player.
 */
public class TeamArmorStand {
    private ArmorStand armorStand;

    public static TeamArmorStand spawn(String name, Color color, Location location) {
        location.getWorld().getEntitiesByClass(ArmorStand.class).stream()
                .filter(e -> location.distanceSquared(e.getLocation()) < 0.5)
                .forEach(Entity::remove);

        ArmorStand armorStand = location.getWorld().spawn(location, ArmorStand.class);
        armorStand.setVisible(false);
        armorStand.setBasePlate(false);
        armorStand.setArms(true);
        armorStand.setCustomName(name);
        armorStand.setCustomNameVisible(true);
        ItemStack head = new ItemStack(Material.SKULL_ITEM, 3, (short) 3);
        SkullMeta skullMeta = (SkullMeta) head.getItemMeta();
        skullMeta.setOwner(ChatColor.stripColor(name));
        head.setItemMeta(skullMeta);
        armorStand.setHelmet(head);

        location.getBlock().setType(Material.BARRIER);
        location.getBlock().getRelative(BlockFace.UP).setType(Material.BARRIER);

        TeamArmorStand teamArmorStand = new TeamArmorStand(armorStand);
        teamArmorStand.setClothes(color);
        return teamArmorStand;
    }

    private TeamArmorStand(ArmorStand armorStand) {
        this.armorStand = armorStand;
    }

    private void lookAt(ArmorStand armorStand, Location target) {
        Location v = armorStand.getLocation().multiply(-1).add(target);
        double r = Math.sqrt(v.getX() * v.getX() + v.getY() * v.getY());
        armorStand.setHeadPose(new EulerAngle(
                Math.atan2(v.getY() * -1.0f, r),
                -Math.atan2(v.getX(), v.getZ()) - Math.toRadians(armorStand.getLocation().getYaw()),
                0));
    }

    private void setClothes(Color color) {
        armorStand.setChestplate(getLeatherArmor(Material.LEATHER_CHESTPLATE, color));
        armorStand.setLeggings(getLeatherArmor(Material.LEATHER_LEGGINGS, color));
        armorStand.setBoots(getLeatherArmor(Material.LEATHER_BOOTS, color));
    }

    public void update() {
        Player nearbyEntity = getNearest(armorStand.getLocation().getWorld().getEntitiesByClass(Player.class), armorStand.getLocation());
        if (nearbyEntity != null) {
            Location a = nearbyEntity.getLocation();
            if (nearbyEntity.isSneaking()) {
                a.subtract(0, 0.125, 0);
            }
            lookAt(armorStand, a);
        }
    }

    public void remove() {
        armorStand.remove();
    }

    private static <T extends Entity> T getNearest(Collection<T> entities, Location location) {
        double min = Double.POSITIVE_INFINITY;
        T nearest = null;

        for (T entity : entities) {
            double distance = location.distanceSquared(entity.getLocation());
            if (distance < min) {
                min = distance;
                nearest = entity;
            }
        }

        return nearest;
    }

    private static ItemStack getLeatherArmor(Material material, Color color) {
        ItemStack item = new ItemStack(material);
        LeatherArmorMeta itemMeta = (LeatherArmorMeta) item.getItemMeta();
        itemMeta.setColor(color);
        item.setItemMeta(itemMeta);
        return item;
    }
}
