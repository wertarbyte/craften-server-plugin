package de.craften.plugins.craften.lobby.components;

import de.craften.plugins.craftenserver.common.components.PluginComponentBase;
import de.craften.plugins.craftenserver.common.util.TitleAPI;
import de.craften.plugins.mcguilib.text.TextBuilder;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class WelcomeTitle extends PluginComponentBase implements Listener {
    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        runTaskLater(new Runnable() {
            @Override
            public void run() {
                TitleAPI.sendTitle(event.getPlayer(), 20, 50, 20,
                        TextBuilder.create("Willkommen").red().getSingleLine(),
                        TextBuilder.create("auf dem Craften Server, " + event.getPlayer().getName()).gold().getSingleLine()
                );
            }
        }, 40);
    }
}
