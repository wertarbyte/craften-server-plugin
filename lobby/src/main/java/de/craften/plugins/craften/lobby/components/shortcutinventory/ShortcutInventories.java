package de.craften.plugins.craften.lobby.components.shortcutinventory;

import de.craften.plugins.craften.lobby.Plugin;
import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.components.PluginComponentBase;
import de.craften.plugins.craftenserver.common.util.CustomSkull;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import java.util.List;

/**
 * A component that gives useful shortcut items to players on join.
 */
public class ShortcutInventories extends PluginComponentBase implements Listener {
    private static final String INVENTORY_BACKUP = "craften-lobby-shortcut-inv_inventory-backup";

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        giveSpecialBlocks(event.getPlayer().getInventory());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        removeItemMenu(event.getPlayer(), false);
    }

    @Override
    protected void onDisabled() {
        for (Player player : getServer().getOnlinePlayers()) {
            removeItemMenu(player, false);
        }
    }

    @EventHandler
    public void onBeforeCommand(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().equalsIgnoreCase("/spawn") || event.getMessage().equalsIgnoreCase("/lobby")) {
            removeItemMenu(event.getPlayer(), false);
            giveSpecialBlocks(event.getPlayer().getInventory());
        }
    }

    private void giveSpecialBlocks(PlayerInventory inventory) {
        {
            ItemStack item = CustomSkull.getSkullBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTVlOGNjOTliYjQyZGRhMmFhZmJmZjQ1Nzc1Njc3NmIyOGM4ZTM0ZWUyNDVjYzU1M2QyNjk0ZTZiMDRiNzIifX19");
            ItemMeta skullMeta = item.getItemMeta();
            skullMeta.setDisplayName("Teleport Block");
            item.setItemMeta(skullMeta);
            inventory.setItem(0, item);
        }
        {
            ItemStack item = CustomSkull.getSkullBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzA5MDEwMmMxNmMyYWVlZjkxZTNkNzdhNWEzNzUwOTZjYzgyNjVjMzRiZjllMTgyMGI4NzFmNDQ0OWZiNmUifX19");
            ItemMeta skullMeta = item.getItemMeta();
            skullMeta.setDisplayName("Gem Shop");
            item.setItemMeta(skullMeta);
            inventory.setItem(1, item);
        }
    }

    private void giveItemMenu(Player player) {
        PlayerInventory inventory = player.getInventory();
        inventory.setItem(0, createBackItem());
        inventory.setItem(1, createBuildWorldItem());
        inventory.setItem(2, createFarmWorldItem());
        inventory.setItem(3, createFarmNetherWorldItem());
        inventory.setItem(4, createCreativeWorldItem());
        inventory.setItem(5, createCreative2WorldItem());
        inventory.setItem(6, createSkyblockItem());
        inventory.setItem(7, createPaintballItem());
        inventory.setItem(8, createGoothaItem());
        inventory.setItem(9, new ItemStack(Material.AIR));
    }

    private void removeItemMenu(Player player, boolean force) {
        List<MetadataValue> inventory = player.getMetadata(INVENTORY_BACKUP);
        player.removeMetadata(INVENTORY_BACKUP, Plugin.getPlugin(Plugin.class));
        if (!inventory.isEmpty()) {
            player.getInventory().setContents((ItemStack[]) inventory.get(0).value());
            //noinspection deprecation
            player.updateInventory();
        } else if (force) {
            player.getInventory().clear();
            giveSpecialBlocks(player.getInventory());
        }
    }

    private ItemStack createBackItem() {
        ItemStack backItem = CustomSkull.getSkullBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzM3NjQ4YWU3YTU2NGE1Mjg3NzkyYjA1ZmFjNzljNmI2YmQ0N2Y2MTZhNTU5Y2U4YjU0M2U2OTQ3MjM1YmNlIn19fQ==");
        ItemMeta meta = backItem.getItemMeta();
        meta.setDisplayName("Back");
        backItem.setItemMeta(meta);
        return backItem;
    }

    private ItemStack createBuildWorldItem() {
        ItemStack serverGuide = new ItemStack(Material.BRICK, 1);
        ItemMeta meta = serverGuide.getItemMeta();
        meta.setDisplayName(ChatColor.BLUE + "Build World");
        serverGuide.setItemMeta(meta);
        return serverGuide;
    }

    private ItemStack createFarmWorldItem() {
        ItemStack serverGuide = new ItemStack(Material.DIAMOND_AXE, 1);
        ItemMeta meta = serverGuide.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GREEN + "Farm World");
        serverGuide.setItemMeta(meta);
        return serverGuide;
    }

    private ItemStack createFarmNetherWorldItem() {
        ItemStack serverGuide = new ItemStack(Material.NETHERRACK, 1);
        ItemMeta meta = serverGuide.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_RED + "Nether");
        serverGuide.setItemMeta(meta);
        return serverGuide;
    }

    private ItemStack createCreativeWorldItem() {
        ItemStack serverGuide = new ItemStack(Material.STAINED_CLAY, 1, (short) 4);
        ItemMeta meta = serverGuide.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "Creative");
        serverGuide.setItemMeta(meta);
        return serverGuide;
    }

    private ItemStack createCreative2WorldItem() {
        ItemStack serverGuide = new ItemStack(Material.STAINED_CLAY, 1, (short) 1);
        ItemMeta meta = serverGuide.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "Creative 2");
        serverGuide.setItemMeta(meta);
        return serverGuide;
    }

    private ItemStack createSkyblockItem() {
        ItemStack serverGuide = new ItemStack(Material.GRASS, 1);
        ItemMeta meta = serverGuide.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Skyblock");
        serverGuide.setItemMeta(meta);
        return serverGuide;
    }

    private ItemStack createPaintballItem() {
        ItemStack serverGuide = new ItemStack(Material.SNOW_BALL, 1);
        ItemMeta meta = serverGuide.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "Paintball");
        serverGuide.setItemMeta(meta);
        return serverGuide;
    }

    private ItemStack createGoothaItem() {
        ItemStack backItem = CustomSkull.getSkullBase64("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvN2VlNDJlOTRmZjI5N2Y3MzM3ZGJjODkzN2UwMTMzN2JkNDE1ZWJjNTg5ZWVjM2EyMTlhNWVjNGUzYTUwZmUxZSJ9fX0=");
        ItemMeta meta = backItem.getItemMeta();
        meta.setDisplayName(ChatColor.GREEN + "Gootha");
        backItem.setItemMeta(meta);
        return backItem;
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (isSpecialItem(event.getItemDrop().getItemStack()) || event.getPlayer().hasMetadata(INVENTORY_BACKUP)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (isSpecialItem(event.getCursor()) ||
                (event.getInventory().getHolder() instanceof Player && ((Player) event.getInventory().getHolder()).getPlayer().hasMetadata(INVENTORY_BACKUP))) {
            event.setCancelled(true);
            //noinspection deprecation
            ((Player) event.getInventory().getHolder()).updateInventory();
        }
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        if (isSpecialItem(event.getCursor()) ||
                (event.getInventory().getHolder() instanceof Player && ((Player) event.getInventory().getHolder()).getPlayer().hasMetadata(INVENTORY_BACKUP))) {
            event.setCancelled(true);
            //noinspection deprecation
            ((Player) event.getInventory().getHolder()).updateInventory();
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Action action = event.getAction();
        if (action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK ||
                action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
            ItemStack item = event.getItem();
            if (item != null && item.hasItemMeta()) {
                String displayName = item.getItemMeta().getDisplayName();
                if (displayName != null) {
                    if (displayName.contains("Teleport Block")) {
                        event.getPlayer().setMetadata(INVENTORY_BACKUP, new FixedMetadataValue(Plugin.getPlugin(Plugin.class), event.getPlayer().getInventory().getContents()));
                        event.getPlayer().getInventory().clear();
                        giveItemMenu(event.getPlayer());
                    } else if (displayName.contains("Gem Shop")) {
                        event.getPlayer().performCommand("gemshop");
                    } else {
                        if (displayName.contains("Back")) {
                            removeItemMenu(event.getPlayer(), true);
                        } else if (displayName.contains("Farm World")) {
                            warp(event.getPlayer(), "farmworld");
                        } else if (displayName.contains("Build World")) {
                            warp(event.getPlayer(), "build");
                        } else if (displayName.contains("Nether")) {
                            warp(event.getPlayer(), "nether");
                        } else if (displayName.contains("Creative 2")) {
                            warp(event.getPlayer(), "creative2spawn");
                        } else if (displayName.contains("Creative")) {
                            warp(event.getPlayer(), "creativespawn");
                        } else if (displayName.contains("Skyblock")) {
                            teleportPlayer(event.getPlayer(), ServerType.SKYBLOCK);
                        } else if (displayName.contains("Paintball")) {
                            teleportPlayer(event.getPlayer(), ServerType.PAINTBALL);
                        } else if (displayName.contains("Gootha")) {
                            teleportPlayer(event.getPlayer(), ServerType.GOOTHA);
                        } else {
                            return;
                        }
                    }
                    event.setCancelled(true);
                }
            }
        }
    }

    private static boolean isSpecialItem(ItemStack item) {
        if (item != null && item.getItemMeta() != null) {
            String itemName = item.getItemMeta().getDisplayName();
            if (itemName != null) {
                if (itemName.contains("Teleport Block") || itemName.contains("Gem Shop")) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Warps the given player to the given warp point, using geSuit Warps. Gives temporary permission to the player,
     * if needed.
     *
     * @param player player to warp
     * @param warp   warp name
     */
    private void warp(Player player, String warp) {
        addTemporaryPermission(player, "gesuit.warps.command.warp", true);
        addTemporaryPermission(player, "gesuit.warps.warp." + warp, true);
        player.performCommand("warp " + warp);
    }
}
