package de.craften.plugins.craften.lobby.components;


import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.components.PluginComponent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class NoMobDamage implements PluginComponent, Listener {
    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onAttack(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && !((Player) event.getDamager()).isOp()) {
            event.setCancelled(true);
        }
    }
}
