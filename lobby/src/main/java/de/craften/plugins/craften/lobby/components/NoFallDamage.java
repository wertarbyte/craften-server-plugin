package de.craften.plugins.craften.lobby.components;

import de.craften.plugins.craftenserver.common.components.PluginComponentBase;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * A module that disables player fall damage everywhere.
 */
public class NoFallDamage extends PluginComponentBase implements Listener {
    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player && event.getCause() == EntityDamageEvent.DamageCause.FALL) {
            event.setCancelled(true);
        }
    }
}
