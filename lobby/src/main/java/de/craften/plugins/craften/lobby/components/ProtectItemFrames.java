package de.craften.plugins.craften.lobby.components;


import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.components.PluginComponent;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.projectiles.ProjectileSource;

public class ProtectItemFrames implements PluginComponent, Listener {
    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onAttack(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Projectile) {
            //Prevent players from killing other player's animals or shooting items out of frames using bows.
            ProjectileSource source = ((Projectile) event.getDamager()).getShooter();
            if (source instanceof Player) {
                Player p = (Player) source;
                if (!p.isOp()) {
                    event.setDamage(0);
                    event.setCancelled(true);
                }
            }
        } else if (!(event.getDamager() instanceof Player && ((Player) event.getDamager()).isOp())) {
            event.setCancelled(true);
        }
    }
}
