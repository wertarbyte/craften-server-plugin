package de.craften.plugins.craften.paintball.components;

import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.components.PluginComponent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class PaintballHandler implements PluginComponent, Listener {
    private CraftenServerPlugin plugin;

    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!player.isOp()) //Automatically join paintball
            plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
                @Override
                public void run() {
                    event.getPlayer().performCommand("pb join");
                }
            }, 20);
    }

    @EventHandler
    public void onBeforeCommand(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().equalsIgnoreCase("/pb leave")) {
            //Go back to the lobby world after leaving paintball
            plugin.teleportPlayer(event.getPlayer(), ServerType.LOBBY);
        }
    }
}
