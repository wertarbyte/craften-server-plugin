package de.craften.plugins.craften.gootha;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.components.BlockMobSpawnerEdit;
import de.craften.plugins.craftenserver.common.components.DisableJoinQuitMessages;
import de.craften.plugins.craftenserver.common.components.MaintenanceMode;
import de.craften.plugins.craftenserver.common.components.commands.ClearCommand;
import de.craften.plugins.craftenserver.common.components.commands.CreativeCommand;
import de.craften.plugins.craftenserver.common.components.commands.PaintballCommand;
import de.craften.plugins.craftenserver.common.components.commands.SpawnCommand;

public class Plugin extends CraftenServerPlugin {
    @Override
    public void onEnable() {
        super.onEnable();

        addComponents(
                new ClearCommand(),
                new SpawnCommand("lobby"),
                new PaintballCommand(),
                new DisableJoinQuitMessages(),
                new CreativeCommand(),
                new MaintenanceMode(),
                new BlockMobSpawnerEdit()
        );
    }

    @Override
    public ServerType getType() {
        return ServerType.GOOTHA;
    }
}
