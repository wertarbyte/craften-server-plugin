package de.craften.plugins.craften.craften;

import de.craften.plugins.craften.craften.components.BuildTeleportCommand;
import de.craften.plugins.craften.craften.components.FarmTeleportCommand;
import de.craften.plugins.craften.craften.components.PlotPriceAddon;
import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.components.*;
import de.craften.plugins.craftenserver.common.components.commands.*;

public class Plugin extends CraftenServerPlugin {
    @Override
    public void onEnable() {
        super.onEnable();

        addComponents(
                new ClearCommand(),
                new SpawnCommand(),
                new PaintballCommand(),
                new SkyblockCommand(),
                new DisableJoinQuitMessages(),
                new CreativeCommand(),
                new MaintenanceMode(),
                new Motd(),
                new PlotPriceAddon(),
                new AfkPlayers(),
                new BlockMobSpawnerEdit(),
                new FarmTeleportCommand(),
                new BuildTeleportCommand()
        );
    }

    @Override
    public ServerType getType() {
        return ServerType.SURVIVAL;
    }
}
