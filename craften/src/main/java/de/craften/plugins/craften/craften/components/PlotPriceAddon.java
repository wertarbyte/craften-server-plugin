package de.craften.plugins.craften.craften.components;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.components.PluginComponent;
import de.craften.plugins.plotplus.PlotPlus;
import de.craften.plugins.plotplus.api.ListeningAddon;
import de.craften.plugins.plotplus.api.events.PlotClaimEvent;


public class PlotPriceAddon extends ListeningAddon implements PluginComponent {
    private static final int FREE_PLOTS = 3;
    private static final int MAXIMUM_PLOT_PRICE = 10_000;
    private static final int INCREASE_PRICE_PER_PLOT = 1_000;

    public PlotPriceAddon() {
        super("Craften Buildworld Addon");
    }

    @Override
    public void onPlotClaiming(PlotClaimEvent event) {
        if (event.getWorld().getName().equalsIgnoreCase("plotme") && !event.getClaimer().hasPermission("plotplus.freeplots")) {
            int plotsByThePlayer = event.getWorld().getPlotsByOwner(event.getOwner().getUniqueId()).size();
            event.setPrice(getPlotPrice(plotsByThePlayer));
        }
    }

    private double getPlotPrice(int plotsByThePlayer) {
        return Math.min(Math.max(INCREASE_PRICE_PER_PLOT * (plotsByThePlayer - FREE_PLOTS + 1), 0), MAXIMUM_PLOT_PRICE);
    }

    @Override
    protected void onEnabled() {
    }

    @Override
    protected void onDisabling() {
    }

    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        PlotPlus.getApi().registerAddon(this);
    }
}
