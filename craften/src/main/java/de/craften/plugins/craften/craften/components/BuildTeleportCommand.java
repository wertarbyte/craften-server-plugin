package de.craften.plugins.craften.craften.components;

import de.craften.plugins.craftenserver.common.components.PluginComponentBase;
import de.craften.plugins.craftenserver.common.util.commands.CommandHandler;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * A command that teleports players to the farm world.
 */
public class BuildTeleportCommand extends PluginComponentBase {
    @Override
    protected void onActivated() {
        registerCommand("build", new CommandHandler() {
            @Override
            public boolean onCommand(CommandSender sender, String command, List<String> args) {
                if (sender instanceof Player) {
                    ((Player) sender).teleport(Bukkit.getWorld("Plotme").getSpawnLocation());
                    return true;
                }
                return false;
            }
        });
    }
}
