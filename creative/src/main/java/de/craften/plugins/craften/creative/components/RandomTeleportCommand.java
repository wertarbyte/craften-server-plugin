package de.craften.plugins.craften.creative.components;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.components.PluginComponent;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Random;

/**
 * A component that adds a /random command that will teleport the player a random distance in a random direction.
 * Credits to CorvusDiabolus for the idea.
 */
public class RandomTeleportCommand implements PluginComponent, CommandExecutor {
    private Random random = new Random();

    @Override
    public void activateFor(CraftenServerPlugin plugin) {
        plugin.getCommand("tprandom").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if (commandSender instanceof Player) {
            Player p = (Player) commandSender;

            Vector direction = new Vector(random.nextDouble() + 1, 0, random.nextDouble() + 1)
                    .normalize().multiply(random.nextDouble() * 1000 + 500);
            Location target = p.getLocation().add(direction);
            target.setY(getTopmostSafeLocation(target.getWorld(), target.getBlockX(), target.getBlockZ()));
            p.teleport(p.getLocation().add(direction));
            return true;
        } else {
            return false;
        }
    }

    private static int getTopmostSafeLocation(World world, int x, int z) {
        for (int y = world.getMaxHeight() - 1; y >= 1; y--) {
            if (world.getBlockAt(x, y, z).getType().isSolid())
                return y + 1;
        }
        return world.getMaxHeight();
    }
}
