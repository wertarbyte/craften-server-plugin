package de.craften.plugins.craften.creative;

import de.craften.plugins.craften.creative.components.RandomTeleportCommand;
import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.components.DisableJoinQuitMessages;
import de.craften.plugins.craftenserver.common.components.AfkPlayers;
import de.craften.plugins.craftenserver.common.components.MaintenanceMode;
import de.craften.plugins.craftenserver.common.components.Motd;
import de.craften.plugins.craftenserver.common.components.commands.ClearCommand;
import de.craften.plugins.craftenserver.common.components.commands.PaintballCommand;
import de.craften.plugins.craftenserver.common.components.commands.SkyblockCommand;
import de.craften.plugins.craftenserver.common.components.commands.SpawnCommand;

public class Plugin extends CraftenServerPlugin {
    @Override
    public void onEnable() {
        super.onEnable();

        addComponents(
                new ClearCommand(),
                new SpawnCommand(),
                new PaintballCommand(),
                new SkyblockCommand(),
                new DisableJoinQuitMessages(),
                new RandomTeleportCommand(),
                new MaintenanceMode(),
                new Motd(),
                new AfkPlayers()
        );
    }

    @Override
    public ServerType getType() {
        return ServerType.CREATIVE;
    }
}
