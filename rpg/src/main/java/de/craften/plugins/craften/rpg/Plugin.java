package de.craften.plugins.craften.rpg;

import de.craften.plugins.craftenserver.common.CraftenServerPlugin;
import de.craften.plugins.craftenserver.common.ServerType;
import de.craften.plugins.craftenserver.common.components.DisableJoinQuitMessages;
import de.craften.plugins.craftenserver.common.components.AfkPlayers;
import de.craften.plugins.craftenserver.common.components.MaintenanceMode;
import de.craften.plugins.craftenserver.common.components.commands.ClearCommand;
import de.craften.plugins.craftenserver.common.components.commands.SpawnCommand;

public class Plugin extends CraftenServerPlugin {
    @Override
    public void onEnable() {
        super.onEnable();

        addComponents(new SpawnCommand());
    }

    @Override
    public ServerType getType() {
        return ServerType.ARCHITECTS;
    }
}
